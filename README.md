# eCommerce Test Task

Test task: Create APIs for the basic eCommerce system.

## Documentation

### API

#### GET api/products

returns JSON list of items.

#### POST api/products

takes JSON of item properties and user' id: {name, price, desc, userId}

inserts product data in database

returns JSON of item properties 

#### GET api/orders/tobuy

takes JSON with user' id: {userId}

returns JSON list of orders, made by user as buyer

#### POST api/orders

takes JSON with user' id and item' id: {userId, itemId}

inserts order with status 'In Progress' in to database

returns JSON of order

#### POST api/orders/reject

takes JSON with user' id and order' id: {userId, orderId}

updates order status to 'Rejected' in database

returns JSON of order
