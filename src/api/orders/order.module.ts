import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { OrderController } from "./order.controller";
import { Order, OrderItem, OrderStatus } from "./order.model";
import { OrderService } from "./order.service";







@Module({
    imports: [
      SequelizeModule.forFeature([
        Order,
        OrderItem,
        OrderStatus,
      ])

    ],
    controllers: [
      OrderController
    ],
    providers: [
      OrderService,

    ],
    exports: [
      SequelizeModule
    ]
  })
  export class OrderModule {}