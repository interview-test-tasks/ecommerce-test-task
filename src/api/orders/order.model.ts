import { AutoIncrement, Column, Model, PrimaryKey, Table } from 'sequelize-typescript';


@Table
export class Order extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  buyerId: number

  @Column
  statusId: number
}

@Table
export class OrderItem extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  itemId: number

  @Column
  orderId: number
}

@Table
export class OrderStatus extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string
}