import { Body, Controller, Delete, Get, Post, Res, } from '@nestjs/common';
import { OrderService } from './order.service';






@Controller('api/orders')
export class OrderController {
    constructor( private readonly orderService: OrderService) {}
  
    @Get('/tobuy')
    async getOrderListByBuyer(@Body() data, @Res() res){
        res.json((await this.orderService.getOrdersByBuyer(data)))
        return res
    }

    @Post('/')
    async postOrder(@Body() data, @Res() res){
        res.json((await this.orderService.makeOrder(data)))
        return res
    }

    @Post('/reject')
    async rejectOrder(@Body() data, @Res() res) {
        res.json((await this.orderService.rejectOrderByBuyer(data)))
        return res
    }
}