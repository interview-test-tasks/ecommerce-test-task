import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Order, OrderItem, OrderStatus } from './order.model';



@Injectable()
export class OrderService {

    private readonly statuses = [
        {
            id: 1,
            name: 'In Process'
        },
        {
            id: 2,
            name: 'Delivered',
        },
        {
            id: 3,
            name: 'Rejected',
        },
      ];

  constructor(
    @InjectModel(Order)
    private orderModel: typeof Order, 
    @InjectModel(OrderItem)
    private orderItemModel: typeof OrderItem, 
    @InjectModel(OrderStatus)
    private orderStatusModel: typeof OrderStatus,  
  ) {}

  async makeOrder({userId, productId}){
      const order = await this.orderModel.create({
          buyerId:userId,
          statusId:1,
      })
      const orderItem = this.orderItemModel.create({
          orderId : order.id,
          itemId : productId,
      })
      return [order, orderItem]
  }

  async getOrdersByBuyer({userId}){
      const orders = await this.orderModel.findAll({
          where:{
              buyerId : userId
          }
      })
      return orders
  }

  async rejectOrderByBuyer({userId, orderId}){
    const order = await this.orderModel.findOne({
        where:{
            id: orderId,
            buyerId : userId
        },
    })
    if (order){
        order.setDataValue('statusId', 3)
        order.save()
        return order
    }else{
        return order
    }
  }
}


