import { Body, Controller, Get, Post, Res, } from '@nestjs/common';
import { ProductService } from './product.service';





@Controller('api/product')
export class ProductController {
  constructor( private readonly productService: ProductService) {}
  
  @Get('/')
  async getProductList(@Res() res){
      res.json((await this.productService.getAllProducts()))
      return res
  }

  @Post('/')
  async PostProduct(@Body() data, @Res() res){
    res.json((await this.productService.putProduct(data)))
    return res
}
}