import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Session } from '../auth/auth.model';
import { Product } from './product.model';


@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product)
    private productModel: typeof Product,  
  ) {}

  async getAllProducts(){
      const products = await this.productModel.findAll()
      return products
  }

  async getProduct({id}){
    const product = await this.productModel.findOne({
        where: {
            id
        }})
    if (product) {
        return product
    }else{
        return false
    }}

  async putProduct({name, price, desc, userId}){
      const product = await this.productModel.create({
          name, 
          price,
          desc,
          sellerId : userId
      })
      if (product){
          return product
      }else{
          return false
      }
  }

  async revokeProduct({id}){
    const result = await this.productModel.destroy({
        where: {
            id,
        }
    })
    if (result){
        return true
    }else{
        return false
    }
  }

}