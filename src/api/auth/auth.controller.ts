import { Body, Controller, Post, Res, } from '@nestjs/common';
import { Response } from 'express';
import { loginForm, registrationForm } from './auth.form';
import { AuthService } from './auth.service';


@Controller('api/auth')
export class AuthController {
  constructor( private readonly authService: AuthService) {}

  @Post('register')
  async postRegister(@Body() data: registrationForm, @Res() res: Response) {
    const isUsernameUsed = await this.authService.validateUsername(data)
    if (!isUsernameUsed){
      const result = await this.authService.registerUser(data);
      if (result) {
        res.status(201)
        res.json({"success":"accountCreated"})
        return res
      }else{
        res.json({"error":"unknownRegistrationError"})
        return res
      }}else{
        res.json({"error":"usernameAlreadyExists"})
        return res
      }
  }

  @Post('login')
  async postLogin(@Body() data: loginForm, @Res() res: Response) {
    const isUserRegistred = await this.authService.validateUsername(data)
    if (isUserRegistred){
      const isUserValid = await this.authService.validateUser(data)
      if (isUserValid){
        const result = await this.authService.loginUser(data)
        if (result){
          res.status(201)
          res.json({
            "success":"userAuthDone", 
            "token":result.token,
          })
          return res
        }else{
          res.json({"error":"unkownLoginError"})
          return res
        }}else{
          res.json({"error":"usernamePasswordPairInvalid"})
          return res
        }}else{
          res.json({"error":"noSuchAccount"})
          return res
        }
      }

  @Post('logout')
  async postLogout(@Body() data, @Res() res: Response) {
    const isSessionValid = await this.authService.validateSession(data)
    if (isSessionValid){
      const result = await this.authService.logoutUser(data);
      if (result) {
        res.status(201)
        res.json({"success":"userAuthUndone"})
        return res
      }else{
        res.json({"error":"unknownLogoutError"})
        return res
      }}else{
        res.json({"error":"noSuchSession"})
        return res
      }
    }
}