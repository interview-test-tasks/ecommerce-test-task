import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Session, User } from './auth.model';


@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User)
    private userModel: typeof User,
    @InjectModel(Session)
    private sessionModel: typeof Session,
  ) {}

  async validateUsername({username}){
    const user = await this.userModel.findOne({
      where: {
        username,
      },
    });
    if (user){
      return true
    }else{
      return false
    }
  }

  async registerUser({username, email, password}){
    
    const result = await this.userModel.create({
      username, 
      email, 
      password})
    return result
  }


  async validateUser({username, password}){
    const user = await this.userModel.findOne({
      where: {
        username,
        password,
      },
    });
    if (user){
      return true
    }else{
      return false
    }
  }

  async loginUser({username, password}) {
    const user = await this.userModel.findOne({
      where: {
        username,
        password,
      },
    })
    const token = user.username
    const session = await this.sessionModel.create({
          userId: user.id,
          token: token
        })
    if (session){
      return session
    }else{
      return false
    }
  }

  async validateSession({token}){
    const session = await this.sessionModel.findOne({
      where: {
        token
      },
    })
    if (session){
      return true
    }else{
      return false
    }
  }

  async logoutUser({token}){
    const session = await this.sessionModel.destroy({
      where: {
        token
      },
    })
    if (session){
      return true
    }else{
      return false
    }
  }

}