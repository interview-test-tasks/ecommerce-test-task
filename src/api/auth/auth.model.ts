import { AutoIncrement, Column, Model, PrimaryKey, Table } from 'sequelize-typescript';


@Table
export class User extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  username: string;

  @Column
  password: string;

  @Column
  email: string;
}

@Table
export class Session extends Model {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  token: string

  @Column
  userId: number;
}
