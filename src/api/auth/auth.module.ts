import { Module } from "@nestjs/common";
import { SequelizeModule } from "@nestjs/sequelize";
import { AuthController } from "./auth.controller";
import { Session, User } from "./auth.model";
import { AuthService } from "./auth.service";





@Module({
    imports: [
      SequelizeModule.forFeature([
        User,
        Session,
      ])

    ],
    controllers: [
      AuthController
    ],
    providers: [
      AuthService,

    ],
    exports: [
      SequelizeModule
    ]
  })
  export class AuthModule {}
  