import { Module } from "@nestjs/common";
import { AuthModule } from "./auth/auth.module";
import { OrderModule } from "./orders/order.module";
import { ProductModule } from "./products/product.module";


@Module({
  imports: [
    AuthModule,
    ProductModule,
    OrderModule,
  ],
  controllers: [

  ],
  providers: [

  ],
})
export class ApiModule {}
