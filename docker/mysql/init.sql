CREATE USER 'test_server'@'localhost' IDENTIFIED BY 'FMBserv_853211';

CREATE DATABASE IF NOT EXISTS test_commerce;

GRANT ALL PRIVILEGES ON fmb_auth . * TO 'test_server'@'localhost';
